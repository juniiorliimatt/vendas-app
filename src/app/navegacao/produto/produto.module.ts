import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CadastroProdutoComponent } from './cadastro-produto/cadastro-produto.component';



@NgModule({
  declarations: [
    CadastroProdutoComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    CadastroProdutoComponent
  ]
})
export class ProdutoModule { }
