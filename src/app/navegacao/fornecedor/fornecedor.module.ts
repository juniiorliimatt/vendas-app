import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CadastroFornecedorComponent } from './cadastro-fornecedor/cadastro-fornecedor.component';

@NgModule({
  declarations: [
    CadastroFornecedorComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    CadastroFornecedorComponent
  ]
})
export class FornecedorModule { }
