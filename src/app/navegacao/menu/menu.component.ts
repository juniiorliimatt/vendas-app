import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { TokenStorageService } from 'src/app/login/token-storage.service';
import { Payload } from 'src/app/models/payload';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  roles: string[] = [];
  isLoggedIn: boolean = false;
  isLoggedOut: boolean = true;
  username: string = '';
  jwtHelper = new JwtHelperService();
  payload: Payload = { sub: '', roles: [] };

  constructor(
    private tokenStorageService: TokenStorageService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorageService.getAccessToken();
    this.getInfo();
  }

  getInfo() {
    var token = this.tokenStorageService.getInfo();
    if (token) {
      this.payload = this.jwtHelper.decodeToken(token);
      this.username = this.payload.sub;
      this.roles = this.payload.roles;
    }
  }

  logout(): void {
    this.tokenStorageService.singOut();
    this.isLoggedIn = false;
    this.isLoggedOut = true;
    this.router.navigate(['login']).then(() => {
      window.location.reload();
    });
  }
}
