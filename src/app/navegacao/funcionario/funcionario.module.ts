import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CadastroFuncionarioComponent } from './cadastro-funcionario/cadastro-funcionario.component';

@NgModule({
  declarations: [
    CadastroFuncionarioComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    CadastroFuncionarioComponent
  ]
})
export class FuncionarioModule { }
