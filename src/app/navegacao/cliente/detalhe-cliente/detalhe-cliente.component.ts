import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Client } from 'src/app/models/client';

import { AtualizarClienteComponent } from '../atualizar-cliente/atualizar-cliente.component';

@Component({
  selector: 'app-detalhe-cliente',
  templateUrl: './detalhe-cliente.component.html',
  styleUrls: ['./detalhe-cliente.component.scss']
})
export class DetalheClienteComponent implements OnInit {
  client!: Client;

  constructor(public dialog: MatDialog, public dialogRef: MatDialogRef<DetalheClienteComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Client) { }

  ngOnInit(): void {
    this.client = this.data;
  }

  update(client: Client) {
    this.dialog.open(AtualizarClienteComponent, { data: { id: client.id, personalData: client.personalData } })
  }

  close(): void {
    this.dialogRef.close();
  }
}
