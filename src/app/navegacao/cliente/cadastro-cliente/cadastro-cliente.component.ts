import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import * as moment from 'moment';
import { City } from 'src/app/models/city';
import { Client } from 'src/app/models/client';
import { Response } from 'src/app/models/response';
import { ResponseError } from 'src/app/models/response-error';
import { State } from 'src/app/models/state';
import { StateService } from 'src/app/services/state.service';

import { CepResponse } from '../../../models/cepResponse';
import { ClientLink } from '../../../models/client-link';
import { CepService } from '../../../services/cep.service';
import { ClienteService } from '../cliente.service';

@Component({
  selector: 'app-cadastro-cliente',
  templateUrl: './cadastro-cliente.component.html',
  styleUrls: ['./cadastro-cliente.component.scss'],
})
export class CadastroClienteComponent implements OnInit {
  clientForm!: FormGroup;
  client!: Client;
  cities: City[] = [];
  states: State[] = [];
  cepResponse: CepResponse = {
    uf: '',
    logradouro: '',
    cep: '',
    bairro: '',
    localidade: '',
    complemento: '',
    ddd: 0,
    gia: 0,
    ibge: 0,
    siafi: 0
  };
  saved: boolean = false;

  horizontalPosition: MatSnackBarHorizontalPosition = 'end'
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';

  constructor(
    private service: ClienteService,
    private stateService: StateService,
    private cepService: CepService,
    private formBuilder: FormBuilder,
    private _snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.findAllStates();
    this.buildform();
  }

  save() {
    if (this.clientForm.valid) {

      const client: Client = {
        id: null,
        personalData: {
          address: {
            complemento: this.clientForm.get('complemento')?.value,
            state: this.clientForm.get('state')?.value,
            city: this.clientForm.get('city')?.value,
            district: this.clientForm.get('district')?.value,
            number: this.clientForm.get('number')?.value,
            street: this.clientForm.get('street')?.value,
            cep: this.clientForm.get('cep')?.value,
          },
          name: this.clientForm.get('name')?.value,
          birthday: moment(this.clientForm.get('birthday')?.value).format('YYYY-MM-DD').toString(),
          comments: this.clientForm.get('comments')?.value,
          cpf: this.clientForm.get('cpf')?.value,
          email: this.clientForm.get('email')?.value,
          phoneNumberOne: this.clientForm.get('phoneNumberOne')?.value,
          phoneNumberTwo: this.clientForm.get('phoneNumberTwo')?.value,
          registrationDate: '',
          rg: this.clientForm.get('rg')?.value,
        },
      };

      /*
       * outra forma de atribuir os itens, mas da erro de null...
       * this.client = Object.assign({}, this.client, this.clientForm.value);
       */

      this.service.save(client).subscribe(
        (response: Response<ClientLink>) => {
          if (response.status === 'CREATED') {
            this.openSnackBar(`Cliente salvo com sucesso!`, 'mat-primary')
            setTimeout(function () {
              window.location.reload();
            }, 1000)
          }
        },
        (error: ResponseError) => {
          this.openSnackBar(`${error.error.detail} - ${error.status}`, 'mat-warn')
          console.log(error);
        }
      );
    }
  }

  getCep() {
    if (this.clientForm.get('cep')?.value.length == 8) {
      this.cepService.get(this.clientForm.get('cep')?.value).subscribe(
        (response: CepResponse) => {
          this.findStateByUf(response.uf);
          this.clientForm.get('city')?.setValue(response.localidade)
          this.clientForm.get('district')?.setValue(response.bairro)
          this.clientForm.get('street')?.setValue(response.logradouro)
        },
        (error: any) => {
          console.log(error);
        }
      )
    }
  }

  private findStateByUf(uf: string) {
    this.stateService.findByUf(uf).subscribe(
      (response: Response<State>) => {
        this.clientForm.get('state')?.setValue(response.data[0].name);
      },
      (error: ResponseError) => {
        console.log(error);
      }
    );
  }

  private openSnackBar(message: string, cor: string) {
    this._snackBar.open(message, '', {
      panelClass: ['mat-toolbar', cor],
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
    });
    setTimeout(() => {
      this._snackBar.dismiss();
    }, 5000)
  }

  private findAllStates() {
    this.stateService.findAll().subscribe(
      (response: Response<State>) => {
        this.states = response.data;
      },
      (error: ResponseError) => {
        console.log(error);
      }
    );
  }

  private buildform() {
    this.clientForm = this.formBuilder.group({
      email: ['', [Validators.email, Validators.required]],
      name: ['', [Validators.required]],
      cpf: ['', [Validators.required, Validators.pattern('[0-9]{3}[.]?[0-9]{3}[.]?[0-9]{3}[-]?[0-9]{2}')]],
      rg: ['', [Validators.required]],
      phoneNumberOne: ['', [Validators.required]],
      phoneNumberTwo: [''],
      birthday: ['', [Validators.required]],
      registrationDate: [''],
      state: [this.cepResponse.uf, [Validators.required]],
      city: ['', [Validators.required]],
      district: ['', [Validators.required]],
      street: ['', [Validators.required]],
      number: ['', [Validators.required]],
      cep: ['', [Validators.required]],
      comments: [''],
    });
  }
}
