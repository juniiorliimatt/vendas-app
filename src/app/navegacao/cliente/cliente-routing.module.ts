import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetalheClienteComponent } from './detalhe-cliente/detalhe-cliente.component';

import { ListarClientesComponent } from './listar-clientes/listar-clientes.component';


const clienteRouterConfig: Routes = [
  { path: 'clientes', component: ListarClientesComponent },
  { path: 'detalhe', component: DetalheClienteComponent }
]

@NgModule({
  imports: [
    RouterModule.forChild(clienteRouterConfig)
  ],
  exports: [RouterModule]
})
export class ClienteRoutingModule { }
