import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { Client } from 'src/app/models/client';
import { ClientLink } from 'src/app/models/client-link';
import { Response } from 'src/app/models/response';
import { ResponseError } from 'src/app/models/response-error';

import { AtualizarClienteComponent } from '../atualizar-cliente/atualizar-cliente.component';
import { CadastroClienteComponent } from '../cadastro-cliente/cadastro-cliente.component';
import { ClienteService } from '../cliente.service';
import { DetalheClienteComponent } from '../detalhe-cliente/detalhe-cliente.component';

@Component({
  selector: 'app-listar-cliente',
  templateUrl: './listar-clientes.component.html',
  styleUrls: ['./listar-clientes.component.scss'],
  providers: [
  ]
})
export class ListarClientesComponent implements OnInit {
  clients: Client[] = [];
  client!: Client;
  paginaAtual: number = 0;
  horizontalPosition: MatSnackBarHorizontalPosition = 'end'
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';

  constructor(private service: ClienteService,
    public dialog: MatDialog,
    private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.findAll();
  }

  findById(id: number) {
    this.service.findById(id).subscribe(
      (response: Response<ClientLink>) => {
        this.client = response.data[0];
      },
      (error: ResponseError) => {
        console.error(error);
      }
    );
  }

  dataSource = new MatTableDataSource(this.clients);
  find(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    this.findByCpf(this.dataSource.filter);
    if (this.dataSource.filter.length <= 0) {
      this.findAll();
    }
  }

  novoCliente() {
    this.dialog.open(CadastroClienteComponent);
  }

  info(client: Client) {
    this.dialog.open(DetalheClienteComponent, { data: { id: client.id, personalData: client.personalData } });
  }

  update(client: Client) {
    this.dialog.open(AtualizarClienteComponent, { data: { id: client.id, personalData: client.personalData } })
  }

  findByCpf(cpf: string) {
    this.service.findByCpf(cpf).subscribe(
      (response: Response<ClientLink>) => {
        this.clients = response.data;
        this.dataSource = new MatTableDataSource(this.clients);
      },
      (error: ResponseError) => {
        console.error(error);
      }
    )
  }

  findAll() {
    return this.service.findAll().subscribe(
      (response: Response<ClientLink>) => {
        this.clients = response.data;
        this.dataSource = new MatTableDataSource(this.clients);
      },
      (error: ResponseError) => {
        console.error(error);
      }
    );
  }

  deleteById(id: number): void {
    this.service.deleteById(id).subscribe(
      (response: Response<Client>) => {
        if (response.status === 'OK') {
          this.openSnackBar(`Deletado com sucesso!`, 'mat-primary')
          setTimeout(function () {
            window.location.reload();
          }, 1000)
        }
      },
      (error: ResponseError) => {
        this.openSnackBar(`${error.error.detail} - ${error.status}`, 'mat-warn')
        console.error(error);
      }
    );
  }

  private openSnackBar(message: string, cor: string) {
    this._snackBar.open(message, '', {
      panelClass: ['mat-toolbar', cor],
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
    });
    setTimeout(() => {
      this._snackBar.dismiss();
    }, 5000)
  }
}
