export interface Response<T> {
  timeStamp: string;
  statusCode: string;
  status: string;
  reason: string;
  message: string;
  developMessage: string;
  data: T[];
}
