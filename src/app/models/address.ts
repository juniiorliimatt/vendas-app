export interface Address {
  district: string | '';
  cep: string | '';
  complemento: string | '',
  city: string | '';
  state: string | '';
  street: string | '';
  number: string | '';
}
