import { Client } from './client';

export interface ClientLink extends Client {
  links: string;
}
