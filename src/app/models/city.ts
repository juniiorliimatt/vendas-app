export interface City {
  id: number;
  uf: string;
  name: string;
}
