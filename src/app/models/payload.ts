export interface Payload {
  sub: string;
  roles: string[];
}
