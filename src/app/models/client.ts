import { PersonalData } from './personal-data';

export interface Client {
  id?: number | any;
  personalData: PersonalData;
}
