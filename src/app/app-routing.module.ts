import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { CadastroClienteComponent } from './navegacao/cliente/cadastro-cliente/cadastro-cliente.component';
import { EstoqueComponent } from './navegacao/estoque/estoque.component';
import { CadastroFornecedorComponent } from './navegacao/fornecedor/cadastro-fornecedor/cadastro-fornecedor.component';
import { CadastroFuncionarioComponent } from './navegacao/funcionario/cadastro-funcionario/cadastro-funcionario.component';
import { HomeComponent } from './navegacao/home/home.component';
import { PerfilComponent } from './navegacao/perfil/perfil.component';
import { CadastroProdutoComponent } from './navegacao/produto/cadastro-produto/cadastro-produto.component';
import { CadastroTransportadoraComponent } from './navegacao/transportadora/cadastro-transportadora/cadastro-transportadora.component';
import { VendasComponent } from './navegacao/vendas/vendas.component';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'estoque', component: EstoqueComponent },
  /**
   * ? forma alternativa { path: 'cliente', loadChildren: () => import('./navegacao/cliente/cliente.module').then(m => m.ClienteModule) },
   * */
  { path: 'cadastro-cliente', component: CadastroClienteComponent },
  { path: 'cadastro-fornecedor', component: CadastroFornecedorComponent },
  { path: 'cadastro-funcionario', component: CadastroFuncionarioComponent },
  { path: 'cadastro-produto', component: CadastroProdutoComponent },
  {
    path: 'cadastro-transportadora',
    component: CadastroTransportadoraComponent,
  },
  { path: 'vendas', component: VendasComponent },
  { path: 'perfil', component: PerfilComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: false })],
  exports: [RouterModule],
})
export class AppRoutingModule { }
