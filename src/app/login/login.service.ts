import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Login } from '../models/login';
import { environment } from 'src/environments/environment';
import { Token } from '../models/token';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private readonly loginUrl = `${environment.LOGIN_URL}`;
  private readonly apiUrl = `${environment.API_URL}`;
  private readonly headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });

  constructor(private http: HttpClient) { }

  singin(login: Login): Observable<Token> {
    let body = `username=${login.username}&password=${login.password}`
    return this.http.post<Token>(`${this.loginUrl}`, body, { headers: this.headers });
  }

  singup(login: Login): Observable<Login> {
    let body = `username=${login.username}&password=${login.password}`
    return this.http.post<Login>(`${this.apiUrl}/user/save`, body, { headers: this.headers })
  }
}
