import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TokenStorageService {
  ACCESS_TOKEN = 'access_token';
  REFRESH_TOKEN = 'refresh_token';
  USER_KEY = 'user_key';

  constructor() { }

  singOut() {
    window.sessionStorage.clear();
  }

  public saveAccessToken(access_token: string): void {
    window.sessionStorage.removeItem(this.ACCESS_TOKEN);
    window.sessionStorage.setItem(this.ACCESS_TOKEN, access_token);
  }

  public saveRefreshToken(refresh_token: string): void {
    window.sessionStorage.removeItem(this.REFRESH_TOKEN);
    window.sessionStorage.setItem(this.REFRESH_TOKEN, refresh_token);
  }

  public getAccessToken(): string | null {
    return window.sessionStorage.getItem(this.ACCESS_TOKEN);
  }

  public getInfo(): string | null {
    return window.sessionStorage.getItem(this.ACCESS_TOKEN);
  }

  public getRefreshToken(): string | null {
    return window.sessionStorage.getItem(this.REFRESH_TOKEN);
  }

  public saveUser(user: any): void {
    window.sessionStorage.removeItem(this.USER_KEY);
    window.sessionStorage.setItem(this.USER_KEY, JSON.stringify(user));
  }

  public getUser(): any {
    const user = window.sessionStorage.getItem(this.USER_KEY);

    if (user) {
      return JSON.parse(user);
    }
    return {}
  }

}
