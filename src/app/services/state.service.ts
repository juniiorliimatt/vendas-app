import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Response } from '../models/response';
import { State } from '../models/state';

@Injectable({
  providedIn: 'root',
})
export class StateService {
  private readonly apiUrl = `${environment.API_URL}`;
  constructor(private http: HttpClient) {}

  findAll(): Observable<Response<State>> {
    return this.http.get<Response<State>>(`${this.apiUrl}/state/find/all`);
  }

  findByName(name: string): Observable<Response<State>> {
    return this.http.get<Response<State>>(
      `${this.apiUrl}/state/find/name/${name}`
    );
  }

  findByUf(uf: string): Observable<Response<State>> {
    return this.http.get<Response<State>>(
        `${this.apiUrl}/state/find/uf/${uf}`
    );
  }
}
