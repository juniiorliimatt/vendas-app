import {TokenStorageService} from './../app/login/token-storage.service';
import {HTTP_INTERCEPTORS, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest,} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

@Injectable()
export class RequestInterceptor implements HttpInterceptor {
    constructor(private tokenService: TokenStorageService) {
    }

    intercept(
        req: HttpRequest<RequestInterceptor>,
        next: HttpHandler
    ): Observable<HttpEvent<any>> {
        let requestInterceptor = req;
        const token = this.tokenService.getAccessToken();
        if (token != null) {
            requestInterceptor = req.clone({
                headers: req.headers.set('Authorization', `Bearer ${token}`),
            });
        }
        return next.handle(requestInterceptor);
    }
}

export const httpInterceptorProviders = [
    {provide: HTTP_INTERCEPTORS, useClass: RequestInterceptor, multi: true},
];
